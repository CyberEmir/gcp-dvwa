# DevSecOps Project 
This is a  DevSecOps project 

## Objectives
- Design and Implementation of DevSecOps architecture 
- Design and Implementation GitLab CI/CD 
- Integration of Google Cloud Platform - GCP expecially Google Kubernetes Engine
- Automating and Performing Static Application Security Testing - SAST
- Automating and Performing Dynamic Application Security Testing - DAST
- Achieving Continuous Security Monitoring
- Integration of ChatOps and Ticketing tools
- Design and Implementation of Google Cloud Security services such as Web Application firewall, Cloud CDN, LoadBalancer, Firewall, Cloud Armor, Cloud Web Security Scanner, Secret Manager,  etc.



## Components and Tools
- GitLab repository:- Code repository and managements
- GitLab CI/CD Pipeline:- for CI/CD Pipeline Implementation
- GKE:- for Application deployment, designing of Dev, staging and production environment
- SonarCube:- for Static code analysis, code quality and Bugs tracking
- JIRA:- For Ticketing and issues tracking
- Slack:- for ChatOps
- ELK Stack: ElasticSearch, Kibana and Fluentd :- for SIEM functionality 
- Prometheus:- for event monitoring and alerting
- Grafana: for interactive visualization
- Auditbeat:- collect auditing data from your hosts
- suricata:- For IDS/IPS

## Links: 
- 
